
const wordsList = [
    {
        eng: 'tussen',
        rus: 'между'
    },
    {
        eng: 'hoge',
        rus: 'высокий'
    },
    {
        eng: 'echt',
        rus: 'действительно'
    },
    {
        eng: 'iets',
        rus: 'что-нибудь'
    },
    {
        eng: 'meerderheid',
        rus: 'большинство'
    },
    {
        eng: 'ander',
        rus: 'другой'
    },
    {
        eng: 'veel',
        rus: 'много'
    },
    {
        eng: 'familie',
        rus: 'семья'
    },
    {
        eng: 'persoonlijk',
        rus: 'личный'
    },
    {
        eng: 'van / uit',
        rus: 'из/вне'
    },
    {
        eng: 'verlaten',
        rus: 'покидать'
    },
];


export default wordsList;