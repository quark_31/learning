import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import HeaderBlock from './components/HeaderBlock';
import Header from './components/Header';
import Paragraph from './components/Paragraph';
import { ReactComponent as ReactLogo } from './logo.svg';
import CardList from './components/CardList';
import  wordsList from './components/Card/wordsList';


const App = () => {
  return (
    <>
        <HeaderBlock>
          <Header>
              Учите Голландский онлайн
          </Header>
          <Paragraph>
              Используйте карточки для запоминания и пополняйте активный словарный запас.
          </Paragraph>
        </HeaderBlock>  
        <CardList item={ wordsList } />
        <HeaderBlock hideBackground>
        <Header>
              Понравилось?
          </Header>

        </HeaderBlock> 
    </>
  ); 
}

export default App;
